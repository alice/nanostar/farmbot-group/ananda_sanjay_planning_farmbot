Supervisor : [Nicolas Drougard](https://pagespro.isae-supaero.fr/nicolas-drougard/?lang=fr)

# Project ALICE (Artificial intelligence for Life In spaCE)

## Planning Strategy of Farmbot

This work is part of ISAE SUPAERO's ALICE (Artificial intelligence for Life In spaCE) project which focuses on developing efficient food production strategies in space using farmbot. The farmbot should be able to autonomously (using AI) minimize the resources and maximize the food production.


<div align="center">

![](Images/Farmbot.png){width=70%}

*Farmbot used in ALICE Project* 
</div>

This repository consists of the files related to planning strategy of the farmbot. The objective of this work is to reduce the computation time of the classical planning algorithm and to start with the probablistic planning. The repository is divided into three parts:

### Replicating Previous Results

Before starting to work on the objective, there was a need to understand the syntax of PDDL (Planning Domain Definition Language) and to follow a correct metholodolgy. Hence, as a first step, the ppdl files (domain and problem) from the senior's ([Lorenzo](https://github.com/lorenzobuizza?tab=repositories)) work were used to understand the working of different planning algorithms like [Fast Forward](https://fai.cs.uni-saarland.de/hoffmann/ff.html), [Metric FF](https://fai.cs.uni-saarland.de/hoffmann/metric-ff.html) and [Fast Downward](https://www.fast-downward.org/HomePage) and the results obtained were compared with the Lorenzo's result.

The source code folder to build the solvers were downloaded from their respective websites and instructions in the readme file (in source code folder) were followed to build the executable solver. The source code files and the executables for each planner are segreggated into different folders and can be found [here](Replicating%20Previous%20Results).

Two pddl files: domain and problem, are used as the input to the solver. The domain and problem pddl files used for each solver are added in their respective folders. Different problem files are created based on the number of waypoints and corresponding plan.txt is genereated using the solvers with following commands:

Fast Forward

`./ff -o domain.pddl -f problem_9.pddl > plan_ff_9.txt`

Metric FF

`./ff -o domain.pddl -f problem_9.pddl -s X > plan_9_opX.txt`

X represents search confirguration ranging from 0 to 5 with 3 giving the optimum plan

Fast Downward

` ./fast-downward.py --alias seq-sat-lama-2011 domain.pddl problem_9.pddl > plan_fd_9.txt`

<div align="center">

![](Images/compres.png){width=70%}

*Comparison of Metric-FF results* 
</div>

The obtained results matched the Lorenzo's result (Metric-FF for example) proving that the planning solvers were correctly built and the methodology used was correct. The computation time of the solver increased with increase in the number of waypoints and the problem was unsolvable for higher waypoints due to large search space and the next part of the work is to reduce the computation time.

### Reducing Computation Time

In the previous section it is seen that with increase in the number of waypoints, the search space is increased leading to more possible combinations of actions and states that need to be explored during the planning process. Therefore the computation time for solving the planning problem also increased and at higher waypoints, the problem became unsolvable. 

<div align="center">

![](Images/graprep.png){width=70%}

*Graphical representation of the configuration of waypoints* 
</div>

<div align="center">

![](Images/modom.png){width=70%}

*Modified domain file* 
</div>

One method used in this work was to reduce the search space by adding the constraint to "move" action such that the robot always moves to the next nearest point. Based on the distances as shown in the graphical representation, the domain file was modified by adding precondition to the move action. The added precondition constraints the movement of farmbot to its nearest surrounding waypoint from its current waypoint. The modified domain file was solved by metric ff and not by fast downward as it doesn't support the "quantified-preconditions" feature. All the files related to "reducing computation time" can be found [here](Reducing%20Computational%20Time).

<div align="center">

![](Images/cost.png){width=70%}

</div>

<div align="center">

![](Images/compstat.png){width=70%}

*Results of original domain vs modified domain* 
</div>

<div align="center">

![](Images/gracomp.png)

*Graphical representation of plan corresponding to the action takedatabefore obtained using Metric-FF solver for 12 + 3 waypoints (Left - Original domain, Right - Modified domain)* 
</div>

<div align="center">

![](Images/gra33.png){width=70%}

*Graphical representation of plan corresponding to the action takedatabefore obtained using Metric-FF solver for 33 + 3 waypoints (modified domain)* 
</div>

The results shows by adding the constraint to the "move" action in modified domain, the search space (number of states to be evaluated) and hence the computation time has exponentially reduced. But still the planning cost of modified domain is close to the original domain. Hence, it can be concluded that the modified domain can be used for next step as it can be used to solve higher number of waypoints at very lower time.

### Perspectives: Probabilistic Planning

Previous sections dealt with classical or deterministic planning. But to include realistic environmental conditions, it is advisable to use probablistic or stochastic planning. Modelling the Farmbot system with the stochastic nature of the environment, such as unpredictable weather patterns, pest infestations or changes in the growth of the plants would be extremely helpful in understanding how the
Farmbot could make decisions to respond to these evolutions. In fact, by incorporating probabilistic
elements into the model, the robot can adapt its decision-making process to respond with flexibility
to such changes.

#### Launching Prost Planner

RDDL (Relational Dynamic Influence Diagram Language) is an extension of PDDL which allows to include the probablistic features to farmbot planning model. [Prost](https://github.com/prost-planner/prost) is one of the planner used to solve RDDL files and as a first step in probabilistic planning, source code from the prost website was downloaded and the planner was built by following the instructions in the website its planning options were explored.

The source code and other files used for building the planner can be found [here](Launching%20Prost%20Planner). Below are the few commands used to solve a planning problem using prost planner.

Step 1: Rddlsim server in prost/testbed folder was launched from terminal 1. This loaded the instances of the planning problem into rddlsim

`./run-server.py -b benchmarks/<planning problem folder>`

Step 2: Ran the planner in prost folder from terminal 2

`./prost.py <instance name> "[Prost options]"`

There were several prost options (found by running prost.py without additional parameters) available and for practice, standard options like [IPC2011], [IPC2014] [UCTStart] were used.

Next step is to reproduce the problem written in PDDL (modified domain in previous section) to rddl.

### Conclusion

In summary, the contributions of this work:
- Replication of previous planning results.
- Problem relaxation to reduce the computation time and thereby making the problem solvable.
- Report results in terms of planning cost, computation time and the number of evaluated states.
- Consider probabilistic planning for future work.


