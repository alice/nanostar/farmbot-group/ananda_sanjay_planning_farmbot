
ff: parsing domain file
domain 'FARMBOT' defined
 ... done.
ff: parsing problem file
problem 'MOTIONANDPICTURE' defined
 ... done.



ff: search configuration is weighted A* with weight 5.
Metric is ((1.00*[RF0](TOTAL-COST)) - () + 0.00)
COST MINIMIZATION DONE (WITH cost-minimizing relaxed plans).

advancing to goal distance:   40
                              39
                              38
                              37
                              36
                              35
                              34
                              33
                              32
                              31
                              30
                              29
                              28
                              27
                              26
                              25
                              24
                              23
                              22
                              21
                              20
                              19
                              18
                              17
                              16
                              15
                              14
                              13
                              12
                              11
                              10
                               8
                               6
                               5
                               4
                               3
                               2
                               1
                               0

ff: found legal plan as follows
step    0: MOVE FARMBOT Z6 Z0
        1: MOUNTSOILSENSOR FARMBOT
        2: MOVE FARMBOT Z0 A2
        3: TAKEDATABEFORE FARMBOT A2
        4: MOVE FARMBOT A2 A1
        5: TAKEDATABEFORE FARMBOT A1
        6: MOVE FARMBOT A1 A0
        7: TAKEDATABEFORE FARMBOT A0
        8: MOVE FARMBOT A0 B0
        9: TAKEDATABEFORE FARMBOT B0
       10: MOVE FARMBOT B0 B1
       11: TAKEDATABEFORE FARMBOT B1
       12: MOVE FARMBOT B1 B2
       13: TAKEDATABEFORE FARMBOT B2
       14: MOVE FARMBOT B2 C2
       15: TAKEDATABEFORE FARMBOT C2
       16: MOVE FARMBOT C2 C1
       17: TAKEDATABEFORE FARMBOT C1
       18: MOVE FARMBOT C1 C0
       19: TAKEDATABEFORE FARMBOT C0
       20: MOVE FARMBOT C0 B1
       21: MOVE FARMBOT B1 A2
       22: MOVE FARMBOT A2 Z0
       23: DISMOUNTSOILSENSOR FARMBOT
       24: MOVE FARMBOT Z0 Z1
       25: MOUNTWATERNOZZLE FARMBOT
       26: MOVE FARMBOT Z1 A2
       27: WATERSOIL FARMBOT A2
       28: MOVE FARMBOT A2 A1
       29: WATERSOIL FARMBOT A1
       30: MOVE FARMBOT A1 A0
       31: WATERSOIL FARMBOT A0
       32: MOVE FARMBOT A0 B0
       33: WATERSOIL FARMBOT B0
       34: MOVE FARMBOT B0 B1
       35: WATERSOIL FARMBOT B1
       36: MOVE FARMBOT B1 B2
       37: WATERSOIL FARMBOT B2
       38: MOVE FARMBOT B2 C2
       39: WATERSOIL FARMBOT C2
       40: MOVE FARMBOT C2 C1
       41: WATERSOIL FARMBOT C1
       42: MOVE FARMBOT C1 C0
       43: WATERSOIL FARMBOT C0
       44: MOVE FARMBOT C0 B1
       45: MOVE FARMBOT B1 A2
       46: MOVE FARMBOT A2 Z1
       47: DISMOUNTWATERNOZZLE FARMBOT
       48: MOVE FARMBOT Z1 Z0
       49: MOUNTSOILSENSOR FARMBOT
       50: MOVE FARMBOT Z0 A2
       51: TAKEDATAAFTER FARMBOT A2
       52: MOVE FARMBOT A2 A1
       53: TAKEDATAAFTER FARMBOT A1
       54: MOVE FARMBOT A1 B0
       55: TAKEDATAAFTER FARMBOT B0
       56: MOVE FARMBOT B0 C1
       57: TAKEDATAAFTER FARMBOT C1
       58: MOVE FARMBOT C1 B2
       59: TAKEDATAAFTER FARMBOT B2
       60: MOVE FARMBOT B2 B1
       61: TAKEDATAAFTER FARMBOT B1
       62: MOVE FARMBOT B1 A0
       63: TAKEDATAAFTER FARMBOT A0
       64: MOVE FARMBOT A0 B1
       65: MOVE FARMBOT B1 C2
       66: TAKEDATAAFTER FARMBOT C2
       67: MOVE FARMBOT C2 B1
       68: MOVE FARMBOT B1 C0
       69: TAKEDATAAFTER FARMBOT C0
       70: MOVE FARMBOT C0 B1
       71: MOVE FARMBOT B1 A2
       72: MOVE FARMBOT A2 Z0
       73: DISMOUNTSOILSENSOR FARMBOT
       74: MOVE FARMBOT Z0 Z6
plan cost: 43554.000000

time spent:    0.00 seconds instantiating 463 easy, 0 hard action templates
               0.00 seconds reachability analysis, yielding 42 facts and 91 actions
               0.00 seconds creating final representation with 42 relevant facts, 1 relevant fluents
               0.00 seconds computing LNF
               0.00 seconds building connectivity graph
               0.00 seconds searching, evaluating 418 states, to a max depth of 0
               0.00 seconds total time

