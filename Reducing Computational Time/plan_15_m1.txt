
ff: parsing domain file
domain 'FARMBOT' defined
 ... done.
ff: parsing problem file
problem 'MOTIONANDPICTURE' defined
 ... done.



ff: search configuration is weighted A* with weight 5.
Metric is ((1.00*[RF0](TOTAL-COST)) - () + 0.00)
COST MINIMIZATION DONE (WITH cost-minimizing relaxed plans).

advancing to goal distance:   64
                              63
                              62
                              61
                              60
                              59
                              58
                              57
                              56
                              55
                              54
                              53
                              52
                              51
                              50
                              49
                              48
                              47
                              46
                              45
                              44
                              43
                              42
                              41
                              40
                              39
                              38
                              37
                              36
                              35
                              34
                              33
                              32
                              31
                              30
                              29
                              28
                              27
                              26
                              25
                              24
                              23
                              22
                              21
                              20
                              19
                              18
                              17
                              16
                              15
                              14
                              13
                              12
                              11
                              10
                               9
                               8
                               7
                               6
                               5
                               4
                               3
                               2
                               1
                               0

ff: found legal plan as follows
step    0: MOVE FARMBOT Z6 Z0
        1: MOUNTSOILSENSOR FARMBOT
        2: MOVE FARMBOT Z0 A2
        3: TAKEDATABEFORE FARMBOT A2
        4: MOVE FARMBOT A2 A1
        5: TAKEDATABEFORE FARMBOT A1
        6: MOVE FARMBOT A1 A0
        7: TAKEDATABEFORE FARMBOT A0
        8: MOVE FARMBOT A0 B0
        9: TAKEDATABEFORE FARMBOT B0
       10: MOVE FARMBOT B0 B1
       11: TAKEDATABEFORE FARMBOT B1
       12: MOVE FARMBOT B1 B2
       13: TAKEDATABEFORE FARMBOT B2
       14: MOVE FARMBOT B2 C2
       15: TAKEDATABEFORE FARMBOT C2
       16: MOVE FARMBOT C2 C1
       17: TAKEDATABEFORE FARMBOT C1
       18: MOVE FARMBOT C1 C0
       19: TAKEDATABEFORE FARMBOT C0
       20: MOVE FARMBOT C0 D0
       21: TAKEDATABEFORE FARMBOT D0
       22: MOVE FARMBOT D0 D1
       23: TAKEDATABEFORE FARMBOT D1
       24: MOVE FARMBOT D1 D2
       25: TAKEDATABEFORE FARMBOT D2
       26: MOVE FARMBOT D2 E2
       27: TAKEDATABEFORE FARMBOT E2
       28: MOVE FARMBOT E2 E1
       29: TAKEDATABEFORE FARMBOT E1
       30: MOVE FARMBOT E1 E0
       31: TAKEDATABEFORE FARMBOT E0
       32: MOVE FARMBOT E0 D0
       33: MOVE FARMBOT D0 C1
       34: MOVE FARMBOT C1 B1
       35: MOVE FARMBOT B1 A2
       36: MOVE FARMBOT A2 Z0
       37: DISMOUNTSOILSENSOR FARMBOT
       38: MOVE FARMBOT Z0 Z1
       39: MOUNTWATERNOZZLE FARMBOT
       40: MOVE FARMBOT Z1 A2
       41: WATERSOIL FARMBOT A2
       42: MOVE FARMBOT A2 A1
       43: WATERSOIL FARMBOT A1
       44: MOVE FARMBOT A1 A0
       45: WATERSOIL FARMBOT A0
       46: MOVE FARMBOT A0 B0
       47: WATERSOIL FARMBOT B0
       48: MOVE FARMBOT B0 B1
       49: WATERSOIL FARMBOT B1
       50: MOVE FARMBOT B1 B2
       51: WATERSOIL FARMBOT B2
       52: MOVE FARMBOT B2 C2
       53: WATERSOIL FARMBOT C2
       54: MOVE FARMBOT C2 C1
       55: WATERSOIL FARMBOT C1
       56: MOVE FARMBOT C1 C0
       57: WATERSOIL FARMBOT C0
       58: MOVE FARMBOT C0 D0
       59: WATERSOIL FARMBOT D0
       60: MOVE FARMBOT D0 D1
       61: WATERSOIL FARMBOT D1
       62: MOVE FARMBOT D1 D2
       63: WATERSOIL FARMBOT D2
       64: MOVE FARMBOT D2 E2
       65: WATERSOIL FARMBOT E2
       66: MOVE FARMBOT E2 E1
       67: WATERSOIL FARMBOT E1
       68: MOVE FARMBOT E1 E0
       69: WATERSOIL FARMBOT E0
       70: MOVE FARMBOT E0 D1
       71: MOVE FARMBOT D1 C1
       72: MOVE FARMBOT C1 B2
       73: MOVE FARMBOT B2 A2
       74: MOVE FARMBOT A2 Z1
       75: DISMOUNTWATERNOZZLE FARMBOT
       76: MOVE FARMBOT Z1 Z0
       77: MOUNTSOILSENSOR FARMBOT
       78: MOVE FARMBOT Z0 A2
       79: TAKEDATAAFTER FARMBOT A2
       80: MOVE FARMBOT A2 A1
       81: TAKEDATAAFTER FARMBOT A1
       82: MOVE FARMBOT A1 B0
       83: TAKEDATAAFTER FARMBOT B0
       84: MOVE FARMBOT B0 C1
       85: TAKEDATAAFTER FARMBOT C1
       86: MOVE FARMBOT C1 D2
       87: TAKEDATAAFTER FARMBOT D2
       88: MOVE FARMBOT D2 E2
       89: TAKEDATAAFTER FARMBOT E2
       90: MOVE FARMBOT E2 E1
       91: TAKEDATAAFTER FARMBOT E1
       92: MOVE FARMBOT E1 D2
       93: MOVE FARMBOT D2 C2
       94: TAKEDATAAFTER FARMBOT C2
       95: MOVE FARMBOT C2 B1
       96: TAKEDATAAFTER FARMBOT B1
       97: MOVE FARMBOT B1 A0
       98: TAKEDATAAFTER FARMBOT A0
       99: MOVE FARMBOT A0 B0
      100: MOVE FARMBOT B0 C0
      101: TAKEDATAAFTER FARMBOT C0
      102: MOVE FARMBOT C0 D1
      103: TAKEDATAAFTER FARMBOT D1
      104: MOVE FARMBOT D1 D0
      105: TAKEDATAAFTER FARMBOT D0
      106: MOVE FARMBOT D0 D1
      107: MOVE FARMBOT D1 C2
      108: MOVE FARMBOT C2 B2
      109: TAKEDATAAFTER FARMBOT B2
      110: MOVE FARMBOT B2 C1
      111: MOVE FARMBOT C1 D0
      112: MOVE FARMBOT D0 E0
      113: TAKEDATAAFTER FARMBOT E0
      114: MOVE FARMBOT E0 D1
      115: MOVE FARMBOT D1 C2
      116: MOVE FARMBOT C2 B2
      117: MOVE FARMBOT B2 A2
      118: MOVE FARMBOT A2 Z0
      119: DISMOUNTSOILSENSOR FARMBOT
      120: MOVE FARMBOT Z0 Z6
plan cost: 64084.000000

time spent:    0.00 seconds instantiating 1021 easy, 0 hard action templates
               0.00 seconds reachability analysis, yielding 66 facts and 151 actions
               0.00 seconds creating final representation with 66 relevant facts, 1 relevant fluents
               0.00 seconds computing LNF
               0.00 seconds building connectivity graph
               0.02 seconds searching, evaluating 1412 states, to a max depth of 0
               0.02 seconds total time

