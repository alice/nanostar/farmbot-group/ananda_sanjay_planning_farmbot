(define
	(problem motionandpicture)
	(:domain farmbot)
	(:objects
		farmbot - robot
		z0 z1 z6 - location	; z6 is the base, z0 is the slot of the soil sensor, z1 is the slot of the watering nozzle
		a0 a1 a2 b0 b1 b2 c0 c1 c2 d0 d1 d2 e0 e1 e2 f0 f1 f2 g0 g1 g2 h0 h1 h2 i0 i1 i2 j0 j1 j2 k0 k1 k2 - waypoint
	)

	(:init 
		(at farmbot z6)		;initial conditions of the robot and the tools
		(bare farmbot)
	)


	(:goal 
		(and (forall (?wp - waypoint) (takendataafter farmbot ?wp))
		     (at farmbot z6)
		     (bare farmbot)
		)
	)

)
