(define
	(problem motionandpicture)
	(:domain farmbot)
	(:objects
		farmbot - robot
		z0 z1 z6 - location	; z6 is the base, z0 is the slot of the soil sensor, z1 is the slot of the watering nozzle
		a0 a1 a2 b0 b1 b2 c0 c1 c2 - waypoint
	)

	(:init 
		(at farmbot z6)		;initial conditions of the robot and the tools
		(bare farmbot)
	)


	(:goal 
		(and (forall (?wp - waypoint) (takendataafter farmbot ?wp))
		     (at farmbot z6)
		     (bare farmbot)
		)
	)
	
)
