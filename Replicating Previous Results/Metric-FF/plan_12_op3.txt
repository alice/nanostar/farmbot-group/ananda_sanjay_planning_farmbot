
ff: parsing domain file
domain 'FARMBOT' defined
 ... done.
ff: parsing problem file
problem 'MOTIONANDPICTURE' defined
 ... done.



ff: search configuration is weighted A* with weight 5.
Metric is ((1.00*[RF0](TOTAL-COST)) - () + 0.00)
COST MINIMIZATION DONE (WITH cost-minimizing relaxed plans).

advancing to goal distance:   52
                              51
                              50
                              49
                              48
                              47
                              46
                              45
                              44
                              43
                              42
                              41
                              40
                              39
                              38
                              37
                              36
                              35
                              34
                              33
                              32
                              31
                              30
                              29
                              28
                              27
                              26
                              25
                              24
                              23
                              22
                              21
                              20
                              19
                              18
                              17
                              16
                              15
                              14
                              13
                              12
                              11
                              10
                               9
                               8
                               7
                               6
                               5
                               4
                               3
                               2
                               1
                               0

ff: found legal plan as follows
step    0: MOVE FARMBOT Z6 Z0
        1: MOUNTSOILSENSOR FARMBOT
        2: MOVE FARMBOT Z0 A2
        3: TAKEDATABEFORE FARMBOT A2
        4: MOVE FARMBOT A2 B2
        5: TAKEDATABEFORE FARMBOT B2
        6: MOVE FARMBOT B2 B1
        7: TAKEDATABEFORE FARMBOT B1
        8: MOVE FARMBOT B1 C1
        9: TAKEDATABEFORE FARMBOT C1
       10: MOVE FARMBOT C1 C2
       11: TAKEDATABEFORE FARMBOT C2
       12: MOVE FARMBOT C2 D2
       13: TAKEDATABEFORE FARMBOT D2
       14: MOVE FARMBOT D2 D1
       15: TAKEDATABEFORE FARMBOT D1
       16: MOVE FARMBOT D1 D0
       17: TAKEDATABEFORE FARMBOT D0
       18: MOVE FARMBOT D0 C0
       19: TAKEDATABEFORE FARMBOT C0
       20: MOVE FARMBOT C0 B0
       21: TAKEDATABEFORE FARMBOT B0
       22: MOVE FARMBOT B0 A0
       23: TAKEDATABEFORE FARMBOT A0
       24: MOVE FARMBOT A0 A1
       25: TAKEDATABEFORE FARMBOT A1
       26: MOVE FARMBOT A1 Z0
       27: DISMOUNTSOILSENSOR FARMBOT
       28: MOVE FARMBOT Z0 Z1
       29: MOUNTWATERNOZZLE FARMBOT
       30: MOVE FARMBOT Z1 A1
       31: WATERSOIL FARMBOT A1
       32: MOVE FARMBOT A1 A0
       33: WATERSOIL FARMBOT A0
       34: MOVE FARMBOT A0 B0
       35: WATERSOIL FARMBOT B0
       36: MOVE FARMBOT B0 C0
       37: WATERSOIL FARMBOT C0
       38: MOVE FARMBOT C0 D0
       39: WATERSOIL FARMBOT D0
       40: MOVE FARMBOT D0 D1
       41: WATERSOIL FARMBOT D1
       42: MOVE FARMBOT D1 D2
       43: WATERSOIL FARMBOT D2
       44: MOVE FARMBOT D2 C2
       45: WATERSOIL FARMBOT C2
       46: MOVE FARMBOT C2 C1
       47: WATERSOIL FARMBOT C1
       48: MOVE FARMBOT C1 B1
       49: WATERSOIL FARMBOT B1
       50: MOVE FARMBOT B1 B2
       51: WATERSOIL FARMBOT B2
       52: MOVE FARMBOT B2 A2
       53: WATERSOIL FARMBOT A2
       54: MOVE FARMBOT A2 Z1
       55: DISMOUNTWATERNOZZLE FARMBOT
       56: MOVE FARMBOT Z1 Z0
       57: MOUNTSOILSENSOR FARMBOT
       58: MOVE FARMBOT Z0 B1
       59: TAKEDATAAFTER FARMBOT B1
       60: MOVE FARMBOT B1 C1
       61: TAKEDATAAFTER FARMBOT C1
       62: MOVE FARMBOT C1 B2
       63: TAKEDATAAFTER FARMBOT B2
       64: MOVE FARMBOT B2 A1
       65: TAKEDATAAFTER FARMBOT A1
       66: MOVE FARMBOT A1 C0
       67: TAKEDATAAFTER FARMBOT C0
       68: MOVE FARMBOT C0 B0
       69: TAKEDATAAFTER FARMBOT B0
       70: MOVE FARMBOT B0 C2
       71: TAKEDATAAFTER FARMBOT C2
       72: MOVE FARMBOT C2 A2
       73: TAKEDATAAFTER FARMBOT A2
       74: MOVE FARMBOT A2 D1
       75: TAKEDATAAFTER FARMBOT D1
       76: MOVE FARMBOT D1 A0
       77: TAKEDATAAFTER FARMBOT A0
       78: MOVE FARMBOT A0 B0
       79: MOVE FARMBOT B0 D0
       80: TAKEDATAAFTER FARMBOT D0
       81: MOVE FARMBOT D0 D2
       82: TAKEDATAAFTER FARMBOT D2
       83: MOVE FARMBOT D2 Z0
       84: DISMOUNTSOILSENSOR FARMBOT
       85: MOVE FARMBOT Z0 Z6
plan cost: 52742.000000

time spent:    0.00 seconds instantiating 265 easy, 0 hard action templates
               0.00 seconds reachability analysis, yielding 54 facts and 265 actions
               0.00 seconds creating final representation with 54 relevant facts, 1 relevant fluents
               0.00 seconds computing LNF
               0.00 seconds building connectivity graph
            2935.54 seconds searching, evaluating 1828369 states, to a max depth of 0
            2935.54 seconds total time

